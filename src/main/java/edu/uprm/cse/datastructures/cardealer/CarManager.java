package edu.uprm.cse.datastructures.cardealer;


import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.JsonError;
import edu.uprm.cse.datastructures.cardealer.util.NotFoundException;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

@Path("cars")
public class CarManager {
	private final SortedList<Car> CARS = CarList.getInstance();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		//checking if cars exist in our list, if not then display an error
		if(this.CARS.isEmpty()) {
		      throw new NotFoundException(new JsonError("Error", "There are no cars found"));
		}
		
		
		
		//converting the SortedList to Array
		Car[] cArray = new Car[this.CARS.size()];
		for (int i =0;i<this.CARS.size();i++) {
			cArray[i] = this.CARS.get(i);
		}
		return cArray;
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {
		if(this.CARS.isEmpty()) { //check existence of cars
			throw new NotFoundException(new JsonError("Error", "There are no cars in your dealer"));
		}
		
		for(Car c:this.CARS) {
			if(c.getCarId() == id) {
				return c;
			}
		}
		
		//if code reaches here, car not found
		throw new NotFoundException(new JsonError("Error", "Car " + id + " not found"));
		
	}
	
	
    @POST
    @Path("/add")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addCar(Car car) { 
    	//Cars can't have invalid parameters
    	if (car.getCarBrand() == null ||
    		car.getCarId() < 0 ||
    		car.getCarModel() == null ||
    		car.getCarModelOption() == null) 
    	{
        	return Response.notModified("Car object invalid").build();

    	}
    	
    	//cannot add car if carId is already in use
    	if(!this.CARS.isEmpty()) {
	    	for(Car c: this.CARS) {
	    		if(c.getCarId() == car.getCarId()) {
	    			return Response.status(409).build();	
	    		}
	    	}
    	}
    	
    	if(this.CARS.add(car)) {
        	return Response.status(201).build();
    	}
    	else return Response.notModified("This object could not be added to your dealer as a car").build();
    }
    
    @PUT
    @Path("/{id}/update")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCar(@PathParam("id") long id, Car car) { 
    	//Cars can't have invalid parameters
    	if (car.getCarBrand() == null ||
    		car.getCarId() < 0 ||
    		car.getCarModel() == null ||
    		car.getCarModelOption() == null) 
    	{
    		return Response.status(404).build();

    	}

    	
    	for(Car c:this.CARS) {
    		if(c.getCarId() == car.getCarId() && c.getCarId() == id) {
    			if(this.CARS.remove(c) && this.CARS.add(car)) {
    				return Response.status(200).build();
    			}
    		}
    	}
    	
		//if code reaches here, id was not found
        return Response.status(404).build();   	
    }
	
    @DELETE
    @Path("/{id}/delete")
    public Response deleteCar(@PathParam("id") long id) {
		for(Car c:this.CARS) {
			//if found then ctd will no longer be null
			if(c.getCarId() == id) {
				if(this.CARS.remove(c)) { //if removed then return Response
					return Response.status(200).build();
				}
			}
		}
		
		//if code reaches here, id was not found
		return Response.status(404).build();
	    

    }
	

	
	
	
	
	
}
