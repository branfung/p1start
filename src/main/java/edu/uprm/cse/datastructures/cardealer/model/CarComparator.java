package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car> {

	@Override
	public int compare(Car a, Car b) {
		int brand = a.getCarBrand().compareToIgnoreCase(b.getCarBrand());
		int model = a.getCarModel().compareToIgnoreCase(b.getCarModel());
		int options = a.getCarModelOption().compareToIgnoreCase(b.getCarModelOption());
		
		if(brand == 0) {
			if(model == 0) {
				if(options == 0) return 0;
				else if(options > 0) return 1;
				else return -1;
			}
			else if(model > 0) return 1;
			else return -1;
		}
		else if(brand > 0) return 1;
		else return -1;
		
	}

}
