package edu.uprm.cse.datastructures.cardealer.util;

public class JsonError {
	
	//THIS CLASS IS FOR THE NotFoundException JSON ERROR MESSAGE
	 private String type;
	 private String message; 
	 
	  public JsonError(String type, String message){
		    this.type = type;
		    this.message = message;                
	  }

	public String getType() {
		return type;
	}

	public String getMessage() {
		return message;
	}	  
	  
	  
}
