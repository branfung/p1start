package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.SortedCircularDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

public class CarList {

	public static SortedList<Car> cList	= new SortedCircularDoublyLinkedList<Car>(new CarComparator());
	
	public static SortedList<Car> getInstance() {
		return cList;
	}
	
	
	public static void resetCars() {
		cList.clear();
	}

}
