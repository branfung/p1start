package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.Car;

public class SortedCircularDoublyLinkedList<E> implements SortedList<E> {

	
	private class DNode<E> { 
		E element;
		DNode<E> next;
		DNode<E> prev;
		
		public DNode(E e, DNode<E> n, DNode<E> p) {
			element = e;
			next = n;
			prev = p;
		}
		public DNode(E e) {
			this(e,null, null);
		}
		public DNode() {
			this(null,null, null);
		}
		
		
		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public DNode<E> getNext() {
			return next;
		}
		public void setNext(DNode<E> next) {
			this.next = next;
		}
		public DNode<E> getPrev() {
			return prev;
		}
		public void setPrev(DNode<E> prev) {
			this.prev = prev;
		}
		public void clear() {
			this.element =  null;
			this.next = null;
			this.prev = null;
		}
		
		
	}

	private class SCDLLIterator<E> implements Iterator<E>{

		private DNode<E> nextNode;
		
		public SCDLLIterator() {
			nextNode = (DNode<E>) header.getNext();
		}
		
		@Override
		public boolean hasNext() {
			return nextNode != header;
		}

		@Override
		public E next() {
			if (this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
		
	}
	
	private DNode<E> header;
	private int currentSize;
	private Comparator<E> comp;
	
	
	public SortedCircularDoublyLinkedList(Comparator<E> cmp) {
		this.header = new DNode<E>();
		this.currentSize = 0;
		this.comp = cmp;
	}
	
	public SortedCircularDoublyLinkedList() {
		header = new DNode<E>();
		currentSize = 0;
		comp = null;
	}

	
	//auxiliary method
	
	private DNode<E> getPosition(int index){
		int currentPosition=0;
		DNode<E> temp = this.header.getNext();
		
		while(currentPosition != index) {
			temp = temp.getNext();
			currentPosition++;
		}
		return temp;
	
	}
	
	//List methods

	@Override
	public Iterator<E> iterator() {
		return new SCDLLIterator<E>();
	}

	@Override
	public boolean add(E obj) {
		DNode<E> newNode = new DNode<E>(obj, header,header.getPrev());
		if(this.isEmpty()) {
			newNode.setNext(header);
			newNode.setPrev(header);
			header.setNext(newNode);
			header.setPrev(newNode);
			currentSize++;
			return true;

		}
		
		//sort only if ther is a comparator
		if(comp != null) {
			for(DNode<E> temp = this.header.getNext(); temp != header; temp = temp.getNext()) {
				if(comp.compare(temp.getElement(), obj) >= 0) {
					newNode.setNext(temp);
					newNode.setPrev(temp.getPrev());
					temp.getPrev().setNext(newNode);
					temp.setPrev(newNode);
					currentSize++;
					return true;
				}
			}
			
		}
		
		//appends to last position
		//when object is sorted last or comparator is null
		header.getPrev().setNext(newNode);
		header.setPrev(newNode);
		currentSize++;
		return true;
		
	}

	@Override
	public int size() {
		return this.currentSize;
	}

	@Override
	public boolean remove(E obj) {
		int i = this.firstIndex(obj);
		if (i < 0) {
			return false;
		}else {
			this.remove(i);
			return true;
		}	}

	@Override
	public boolean remove(int index) {
		if ((index < 0) || (index >= this.currentSize)){
			throw new IndexOutOfBoundsException();
		}		
		
		DNode<E> ntr = this.getPosition(index);
		DNode<E> next = ntr.getNext();
		ntr.getPrev().setNext(next);
		next.setPrev(ntr.getPrev());
		ntr.clear();
		currentSize--;
		return true;
		
	}

	@Override
	public int removeAll(E obj) {
		int count =0;
		while(this.remove(obj) != false) {
			count++;
		}
		return count;
	}

	@Override
	public E first() {
		return header.getNext().getElement();
	}

	@Override
	public E last() {
		return header.getPrev().getElement();
	}

	@Override
	public E get(int index) {
		if ((index < 0) || (index >= this.currentSize)){
			throw new IndexOutOfBoundsException();
		}		

		return this.getPosition(index).getElement();
	}

	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}
	}

	@Override
	public boolean contains(E e) {
		return this.firstIndex(e) != -1;
	}

	@Override
	public boolean isEmpty() {
		return this.currentSize == 0;
	}

	@Override
	public int firstIndex(E e) {
		int i = 0;
		for (DNode<E> temp = this.header.getNext(); temp != header; 
				temp = temp.getNext(), i++) {
			if (temp.getElement().equals(e)) {
				return i;
			}
		}

		return -1;
	}

	@Override
	public int lastIndex(E e) {
		int i = 0, result = -1;
		for (DNode<E> temp = this.header.getNext(); temp != header; 
				temp = temp.getNext(), i++) {
			if (temp.getElement().equals(e)) {
				result = i;
			}
		}

		return result;	
	}
	

}
